﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{

    private static float speed = 50.0f;
    private static float X_sens = 2.0f;
    private static float Y_sens = 2.0f;

    private float xLBound;
    private float xHBound;
    private float yHBound;
    private float zLBound;
    private float zHBound;






    // Use this for initialization
    void Start()
    {

        this.GetComponent<Rigidbody>().freezeRotation = true;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        this.GetComponent<Rigidbody>().velocity = Vector3.zero;

        this.transform.eulerAngles += new Vector3(-Input.GetAxis("Mouse Y") * Y_sens, Input.GetAxis("Mouse X") * X_sens, 0);

        if (Input.GetKey(KeyCode.W))
            tryMoveTo(transform.TransformDirection(Vector3.forward * speed));
        if (Input.GetKey(KeyCode.S))
            tryMoveTo(transform.TransformDirection(Vector3.back * speed));
        if (Input.GetKey(KeyCode.A))
            tryMoveTo(transform.TransformDirection(Vector3.left * speed));
        if (Input.GetKey(KeyCode.D))
            tryMoveTo(transform.TransformDirection(Vector3.right * speed));
        if (Input.GetKey(KeyCode.Q))
        {
            this.transform.Rotate(Vector3.forward);
        }
        if (Input.GetKey(KeyCode.E))
        {
            this.transform.Rotate(Vector3.back);
        }

    }

    void tryMoveTo(Vector3 newDir)
    {

        Vector3 newPosiWorld = transform.position + newDir;
        if (newPosiWorld.x > xHBound | newPosiWorld.x < xLBound | newPosiWorld.y > yHBound | newPosiWorld.z > zHBound | newPosiWorld.z < zLBound) { }
        else
        {
            this.GetComponent<Rigidbody>().velocity = (newDir);
        }
    }

    public void setBound(float xH, float xL, float yH, float zH, float zL)
    {
        xLBound = xL;
        xHBound = xH;

        yHBound = yH;

        zHBound = zH;
        zLBound = zL;

        

    }


}
