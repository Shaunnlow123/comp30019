﻿using UnityEngine;
using System.Collections;

public class LandScape : MonoBehaviour
{

    public Shader shader;
    public PointLight lightSource;
    public Camera mainCamera;

    public float pixelWidth = 5.0f;        
    public float grain = 1.4f;
    public float MAX_X { get; private set; }
    public float MIN_X { get; private set; }
    public float MAX_Z { get; private set; }
    public float MIN_Z { get; private set; }
    public Color color1, color2, color3, color4;

    private MeshFilter terrainMeshFilter;
    private MeshRenderer terrainRenderer;
    private MeshCollider terrainCollider;

    private float[,] heightmap;
    private Vector3[] vertices;
    private Color[] colors;
    private int[] triangles;
    private int length;
    private float maxHeight;
    private float minHeight;
    private float heightRange;

    void Start()
    {
        terrainMeshFilter = this.gameObject.AddComponent<MeshFilter>();
        terrainMeshFilter.mesh = this.generateTerrain();
        terrainRenderer = this.gameObject.AddComponent<MeshRenderer>();
        terrainRenderer.material.shader = shader;
        terrainCollider = this.gameObject.AddComponent<MeshCollider>();
        terrainCollider.sharedMesh = this.GetComponent<MeshFilter>().mesh;

        mainCamera.GetComponent<CameraController>().setBound(MAX_X, MIN_X, 500, MAX_Z, MIN_Z);
        
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            length = 129;
            //this.gameObject.GetComponent<MeshFilter>().terrainMesh = this.generateTerrain();
            terrainMeshFilter.mesh = this.generateTerrain();
            //terrainMesh = GetComponent<MeshFilter>().terrainMesh;
            terrainCollider.sharedMesh = GetComponent<MeshFilter>().mesh;
            terrainRenderer = this.gameObject.GetComponent<MeshRenderer>();

            //resetCamLight();
        }
        terrainRenderer.material.SetColor("_PointLightColor", this.lightSource.color);
        terrainRenderer.material.SetVector("_PointLightPosition", this.lightSource.GetWorldPosition());

    }
    
    // terrain generator
    private Mesh generateTerrain()
    {
        // 2^7 + 1
        length = 129;

        // Setting initial variables
        int totalPixels = length*length;
        long totalTriangles = (length-1)*(length-1)*6;
        vertices = new Vector3[totalPixels];                                              
        triangles = new int[totalTriangles];   
        colors = new Color[totalPixels];
        heightmap = new float[length, length];
        
        // Maximum and minimum x and z coodinates for terrain
        MAX_X = (length) * pixelWidth / 2;
        MIN_X = -(length) * pixelWidth / 2;
        MAX_Z = MAX_X;
        MIN_Z = MIN_X;

        // Perform Diamond-Square algorithm to generate Heightmap
        dsAlgorithm(length);

        // find minimum and maximum height of all the points in the terrain.
        findHeightBounds();
        generateVertices();
        generateTriangles();
        generateColors();

        // Create mesh for terrain and set its configurations
        Mesh terrainMesh = new Mesh();
        terrainMesh.name = "Terrain";
        terrainMesh.vertices = vertices;
        terrainMesh.triangles = triangles;
        terrainMesh.colors = colors;
        terrainMesh.RecalculateNormals();

        return terrainMesh;
    }

    // Perform Diamond-Square algorithm on an empty matrix with similar configurations
    // to the required heightmap and generate a heightmap.
    private void dsAlgorithm(int height)
    {
        int half = height / 2;

        // Reached minimum size
        if (half < 1) return;

        // Square part of the algorithm
        for (int i = half; i < length; i += height)
        {
            for (int j = half; j < length; j += height)
            {
                heightmap[i, j] = (heightmap[i - half, j - half] +
                                   heightmap[i + half, j - half] +
                                   heightmap[i - half, j + half] +
                                   heightmap[i + half, j + half]) / 4
                                   + Random.Range(-height, height) * grain;
            }
        }

        // Diamond part of the algorithm
        for (int i = 0; i < length; i += half)
        {
            for (int j = (i + half) % height; j < length; j += height)
            {
                heightmap[i, j] = (heightmap[i, Mathf.Max(j - half, 0)] +
                                   heightmap[Mathf.Min(i + half, length - 1), j] +
                                   heightmap[i, Mathf.Min(j + half, length - 1)] +
                                   heightmap[Mathf.Max(i - half, 0), j]) / 4
                                   + Random.Range(-height, height) * grain;
            }
        }

        // Recursion
        dsAlgorithm(height / 2);
    }

    // Find minimum and maximum height based on the generated heightmap
    private void findHeightBounds() {
        minHeight = 0;
        maxHeight = 0;

        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                if (heightmap[i, j] > maxHeight) {
                    maxHeight = heightmap[i, j];
                }
                else if (heightmap[i, j] < minHeight) {
                    minHeight = heightmap[i, j];
                }
            }
        }
        heightRange = maxHeight - minHeight;
    }

    // Generate vertices based on heightmap
    private void generateVertices() {
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                vertices[length*i + j] = new Vector3(i*pixelWidth - length*pixelWidth/2, heightmap[i, j], j*pixelWidth - length*pixelWidth/2);
            }
        }
    }

    // Generate triangles
    private void generateTriangles() {
        // Vertice index
        int k = 0;

        for (int i = 1; i < length; i++) {
            for (int j = 0; j < length - 1; j++) {
                triangles[k] = length*i + j;                
                triangles[k + 1] = length*(i - 1) + j;     
                triangles[k + 2] = length*(i - 1) + (j + 1); 
                triangles[k + 3] = length*i + j;            
                triangles[k + 4] = length*(i - 1) + (j + 1); 
                triangles[k + 5] = length*i + (j + 1);       
                k += 6;
            }
        }
    }

    // Generate colors on the terrain based on height
    private void generateColors() {
        // Distribute textures(colors) over 4 regions
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                // Texture 1 ( Lowest )
                if (heightmap[i,j] < (minHeight + heightRange*0.2))
                {
                    colors[length*i + j] = color1;
                }

                // Texture 2 ( 2nd from bottom )
                else if (heightmap[i, j] < (minHeight + heightRange * 0.5))
                {
                    colors[length*i + j] = color2;
                }

                // Texture 3 ( 2nd from top )
                else if (heightmap[i, j] < (minHeight + heightRange * 0.8))
                {
                    colors[length*i + j] = color3;
                }

                // Texture 4 ( Highest )
                else
                {
                    colors[length*i + j] = color4;
                }
            }
        }
    }

    public float getWaterHeight()
    {
        return minHeight + (heightRange * 0.2f);
    }
}

