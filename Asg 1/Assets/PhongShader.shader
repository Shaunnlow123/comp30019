﻿// Phong shader for terrain
Shader "Unlit/PhongShader"
{
	Properties
	{
		_PointLightColor("Point Light Color", Color) = (0, 0, 0)
		_PointLightPosition("Point Light Position", Vector) = (0.0, 0.0, 0.0)
	}
	SubShader
	{
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			uniform float3 _PointLightColor; 
			uniform float3 _PointLightPosition; 

			struct vertIn				
			{
				float4 vertex : POSITION;
				float4 normal : NORMAL;       
				float4 color : COLOR;
			};

			struct vertOut
			{
				float4 vertex : SV_POSITION;
				float4 normal : NORMAL;
				float4 color : COLOR;
				float4 vertexWorld : TEXCOORD0;
				float3 normalWorld : TEXCOORD1;
			};

			// vertex shader
			vertOut vert(vertIn v)
			{
				vertOut o;

				// convert vertex position and corresponding normal into world coords 
				float4 worldVertex = mul(_Object2World, v.vertex);
				float3 worldNormal = normalize(mul(transpose((float3x3)_World2Object), v.normal.xyz));

				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.color = v.color;
				o.normal = v.normal;
				o.normalWorld = worldNormal;
				o.vertexWorld = worldVertex;

				return o;
			}

			// Implementation of the fragment shader
			fixed4 frag(vertOut v) : SV_Target
			{
				// ambient RGB intensities
				float Ka = 1;
				float3 amb = v.color.rgb * UNITY_LIGHTMODEL_AMBIENT.rgb * Ka;

				// diffuse RBG reflections
				float fAtt = 1;
				float Kd = 1;
				float3 L = normalize(_PointLightPosition - v.vertexWorld.xyz);
				float LdotN = dot(L, v.normalWorld.xyz);
				float3 dif = fAtt * _PointLightColor.rgb * Kd * v.color.rgb * saturate(LdotN);

				// specular reflections
				float Ks = 0.2f;
				float specN = 80; // Values>>1 give tighter highlights
				float3 V = normalize(_WorldSpaceCameraPos - v.vertexWorld.xyz);
				float3 R = 2 * LdotN * v.normalWorld.xyz - L;
				float3 spe = fAtt * _PointLightColor.rgb * Ks * pow(saturate(dot(V, R)), specN);

				// Combine Phong illumination model components
				v.color.rgb = amb.rgb + dif.rgb + spe.rgb;
				v.color.a = v.color.a;

				return v.color;
			}
			ENDCG
		}
	}
}
