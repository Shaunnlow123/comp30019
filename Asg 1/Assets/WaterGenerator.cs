﻿using UnityEngine;
using System.Collections;
public class WaterGenerator : MonoBehaviour
{
    public LandScape landscape;
    public PointLight lightSource;
    public Shader shader1;
    Color waterColor;
    private float waterHeight;
    private MeshFilter wMeshFilter;
    private Mesh wMesh;
    private MeshRenderer wRenderer;
    private MeshCollider wCollider;
    private Material wMaterial;
    // Use this for initialization
    void Start()
    {
        waterColor = new Color(0, 0, 1, 0.5f);
        wMeshFilter = this.gameObject.AddComponent<MeshFilter>();
        wMeshFilter.mesh = this.generateWater();
        wRenderer = this.gameObject.AddComponent<MeshRenderer>();
        wRenderer.material.color = waterColor;
        wCollider = this.gameObject.AddComponent<MeshCollider>();
        wCollider.sharedMesh = this.GetComponent<MeshFilter>().mesh;
        wMaterial = new Material(Shader.Find("Standard"));
        shader1 = Shader.Find("Transparent/Diffuse");

        GetComponent<Renderer>().material = wMaterial;
    }

    void Update()
    {
        wMaterial.SetColor("_Color", waterColor);
        wMeshFilter.mesh = this.generateWater();
        wCollider.sharedMesh = this.GetComponent<MeshFilter>().mesh;
        wRenderer.material.shader = shader1;
    }
    // Generate the water terrain
    private Mesh generateWater()
    {
        Mesh w = new Mesh();
        w.name = "Water";
        waterHeight = landscape.getWaterHeight();
        // Water terrain vertices
        w.vertices = new Vector3[] {
            new Vector3(landscape.MIN_X, landscape.getWaterHeight(), landscape.MIN_X),
            new Vector3(landscape.MIN_X, landscape.getWaterHeight(), landscape.MAX_X),
            new Vector3(landscape.MAX_X, landscape.getWaterHeight(), landscape.MAX_X),
            new Vector3(landscape.MAX_X, landscape.getWaterHeight(), landscape.MIN_X)
        };

        // Water terrain color
        w.colors = new Color[] {
            waterColor,
            waterColor,
            waterColor,
            waterColor
        };

        // Water terrain triangles (Only 2 triangles based on the vertices)
        w.triangles = new int[] { 0, 1, 2, 0, 2, 3 };
        w.RecalculateNormals();
        return w;
    }
}