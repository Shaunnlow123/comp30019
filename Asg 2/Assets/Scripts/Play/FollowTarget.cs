﻿using UnityEngine;
using System.Collections;

namespace RabbitHole {
	public class FollowTarget : MonoBehaviour {

		public Transform target;
		public Vector3 offset = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);

		void FixedUpdate () {
			Vector3 v = transform.position;
			if (offset.x != float.MaxValue)
				v.x = target.position.x + offset.x;
			//if (offset.y != float.MaxValue)
				//v.y = target.position.y + offset.y;
			if (offset.z != float.MaxValue)
				v.z = target.position.z + offset.z;
			transform.position = v;
		}
	}
}