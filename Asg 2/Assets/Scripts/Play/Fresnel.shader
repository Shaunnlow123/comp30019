// Fresnel shader that implements specular lighting with
// the Fresnel effect

Shader "Custom/Fresnel" {
	Properties
	{
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTex ("Main Texture", 2D) = "white" {}
		_SpecColor ("Specular Color", Color) = (1,1,1,1)
		_SpecShininess ("Specular Shine", Range(1.0, 100.0)) = 1.0
		_FresnelPower ("Fresnel Power", Range(0.0, 3.0)) = 1.0
		_FresnelScale ("Fresnel Scale", Range (0.0, 1.0)) = 1.0
		_FresnelColor ("Fresnel Color", Color) = (1,1,1,1)
	}

	SubShader
	{
		Pass
		{  
			Tags { "LightMode" = "ForwardBase" }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct vertIn {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float2 vertexCoord : TEXCOORD0;
			};

			struct vertOut {
				float4 vertex : SV_POSITION;
				float3 normal : NORMAL;
				float2 vertexCoord : TEXCOORD0;
				float4 worldVertex : TEXCOORD1;
			};

			float4 _LightColor0;
			float4 _Color;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _SpecColor;
			float _SpecShininess;
			float _FresnelPower;
			float _FresnelScale;
			float4 _FresnelColor;
			
			vertOut vert(vertIn v)
			{
				vertOut o;
				UNITY_INITIALIZE_OUTPUT(vertOut, o);
				// World vertex coordinates transformed to camera vertex coordinates
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);

				// Convert vertex coordinates into world coordinates
				float4 worldVertex = mul(unity_ObjectToWorld, v.vertex);
				
				// Convert normal coordinates into world coordinates
				float3 normal = mul(float4(v.normal, 0.0), unity_ObjectToWorld).xyz;
				
				o.vertexCoord = TRANSFORM_TEX(v.vertexCoord, _MainTex);

				o.normal = normal;
				o.worldVertex = worldVertex;
				return o;
			}
			
			float4 frag (vertOut v) : COLOR
			{
				// Texture colour
				float4 color = tex2D(_MainTex, v.vertexCoord);

				// Light position
				float3 light = normalize(_WorldSpaceLightPos0.xyz);

				// World normal position
				float3 normal = normalize(v.normal);

				// World view position
         		float3 view = normalize(_WorldSpaceCameraPos - v.worldVertex.xyz);

				// Colour after diffuse
         		float3 diffuse = _LightColor0.rgb * _Color.rgb * max(0.0, dot(normal, light));

				// Specular light
         		float3 specular = float3(0.0, 0.0, 0.0);
	            if (dot(normal, light) >= 0.0) 
	            {
	               specular = _LightColor0.rgb * _SpecColor.rgb * pow(max(0.0, dot(reflect(-light, normal), view)), _SpecShininess);
	            }

	            float3 V = v.worldVertex - _WorldSpaceCameraPos.xyz;

				// Reflection of light 
				// This is the Fresnel effect
				float reflection = _FresnelScale * pow(1.0 + dot(normalize(V), normal), _FresnelPower);

				// Diffused specular lighting
	            float3 diffuseSpecular = diffuse + specular;

				// The final colour of the pixel
				float4 finalColor = float4(diffuseSpecular,1) * color;

				return lerp(finalColor, _FresnelColor, reflection);
			}
		ENDCG
	}
}
Fallback "Diffuse"
}
