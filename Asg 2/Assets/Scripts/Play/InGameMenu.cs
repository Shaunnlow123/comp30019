﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InGameMenu : MonoBehaviour {


    // End game pop up
    public Canvas endgame;
    public Button restart;
    public Button toMainMenu;

    // The player character
    public PlayerContoller pc;

    // The in game display text
    public Text scoreText;

    // The instruction will disappear after this amount of time
    public int instructLifeTime;
    private float lifeCount;
    public Text instruction;

	// Use this for initialization
	void Start () {
        endgame = endgame.GetComponent<Canvas>();
        restart = restart.GetComponent<Button>();
        toMainMenu = toMainMenu.GetComponent<Button>();
        pc = pc.GetComponent<PlayerContoller>();
        instruction.enabled = true;
        lifeCount = 0;


        endgame.enabled = false;
        
	}

    public void Restart()
    {
        SceneManager.LoadScene("main");
    }

    public void toMain()
    {
        SceneManager.LoadScene("MainMenu");
    }
	
	// Update is called once per frame
	void Update () {

        if(lifeCount < instructLifeTime)
        {
            lifeCount++;
        }
        else
        {
            instruction.enabled = false;
        }

        if(pc.gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsTag("Dead"))
        {
            endgame.enabled = true;
        }
        else
        {
            // Display the player score
            scoreText.text = "Your score: " + pc.score;
        }
	
	}
}
