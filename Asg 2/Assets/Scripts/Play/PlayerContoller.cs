﻿using UnityEngine;
using System.Collections;

public class PlayerContoller : MonoBehaviour
{

    // The animation controller for player
    // All player animations are found in www.mixamo.com
    Animator animator;
    CharacterController controller;

    // The speed of player moving forward, left and right, or jump
    public float speed = 10f;
    public float horizontalSpeed = 0.2f;
    public float jumpSpeed = 0.5f;

    public float gravityConstant = -1.7f;
    private Vector3 gravity =Vector3.zero;
    private bool couldBeSwipe = false;
    public float score;

    // Number of diamond picked 
    public int diamond;
    public int diamondScore = 100;
    Vector2 startPos;

    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        controller = GetComponent<CharacterController>();
        score = 0;
        diamond = 0;
    }

    // Update is called once per frame
    void Update()
    {

        // Tracking the player height for end game and animation
        animator.SetFloat("Height", transform.position.y);

        

        // Record if in end game 
        if (animator.GetCurrentAnimatorStateInfo(0).IsTag("Dead")) {
            if (!controller.isGrounded)
            {
                gravity.y += gravityConstant * Time.deltaTime;
            }

            if (transform.position.y < - 20) animator.SetBool("FallOut", true);
            else
            controller.Move(gravity);
        }
        else
        {

            // Player score 
            score = Mathf.Round(this.transform.position.x + diamond * diamondScore);
            //Debug.Log(score);
            Vector3 velocity = Vector3.zero;
            velocity.x = speed * Time.deltaTime;

            // The gravity of the player
            if (!controller.isGrounded)
            {
                gravity.y += gravityConstant * Time.deltaTime;
            }
            else
            {
                gravity = Vector3.zero;

            }

            
            // Detect swipe for player jump
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        couldBeSwipe = true;
                        startPos = touch.position;
                        break;
                    case TouchPhase.Ended:
                        if (couldBeSwipe)
                        {
                            if (Mathf.Abs(startPos.y - touch.position.y) > 2 && Mathf.Sign(touch.position.y - startPos.y) == 1f && transform.position.y < 0.1)
                            {
                                Jump();
                                startPos = Vector2.zero;
                            }
                            couldBeSwipe = false;

                        }
                        break;

                }
            }

            


            

            // The accelerometer input for left and right
            velocity.z = -(Input.acceleration.x * 25f) * Time.deltaTime;
        
            // The keyboard input
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                velocity.z = horizontalSpeed;
            }
            if (Input.GetKey(KeyCode.RightArrow))
                velocity.z = -horizontalSpeed;
            if (Input.GetKeyDown(KeyCode.Space) && transform.position.y < 0.1)
            {
                Jump();
            }



            velocity += gravity;
            controller.Move(velocity);
            
        }
    }
    
    // let the player jump
    void Jump()
    {
        animator.SetTrigger("Jump");
        gravity.y = jumpSpeed;
    }


    // Collide detection for player
    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.gameObject.tag == "Obstacle")
            animator.SetBool("Die", true);

        if (hit.gameObject.tag == "Diamond")
        {
            this.diamond++;
            Object.Destroy(hit.gameObject);
        }
        if(hit.gameObject.tag == "Fire")
        {
            animator.SetBool("Burn", true);
        }
    }
    



}
