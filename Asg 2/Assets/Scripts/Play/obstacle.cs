﻿using UnityEngine;
using System.Collections;
//using Scripts.Play;

public class obstacle : MonoBehaviour
{

    public Texture diffuseMap;
    //rocks found here: https://www.assetstore.unity3d.com/en/#!/content/11256

    void Start()
    {
        MeshRenderer renderer = this.gameObject.GetComponent<MeshRenderer>();
        renderer.material.mainTexture = diffuseMap;
    }
    void Update()
    {
    }
}
