﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class obstacleGenerator : MonoBehaviour
{
    // Obstacles or objects that will spawn in game including rocks/gems
    public GameObject[] Obstacles;
    private int obstacleCount; // Number of obstacles
    public int maxObstacles; // Maximum allowed number of obstacles
    public bool RandomX = false;

    // Min and Max stats of each tile
    public float MIN_Z = -16.5f, MAX_Z = 16.5f;
    public float MIN_X = 0f, MAX_X = 33.0f;
    public Vector3 playerPosition;
    public float playerX;
    public float distanceTravelled;


    // Use this for initialization
    void Start()
    {
        obstacleCount = 0;
        distanceTravelled = 0;
    }

    void Update()
    {
        playerPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
        var playerX2 = GameObject.FindGameObjectWithTag("Player").transform.position.x;
        distanceTravelled += (playerX2 - playerX);
        playerX = playerX2;
        // Debug.Log(distanceTravelled);
        if (Input.GetKeyDown(KeyCode.C) || distanceTravelled > 10)
        {
            distanceTravelled = 0;
            randomObject();
        }


    }

    // Randomises location of each obstacle
    void randomObject()
    {
     
        var randomX = Random.Range(5.0f, 10.0f);
        var randomZ = Random.Range(-4.5f, 4.5f);
        var newObstacle = Obstacles[Random.Range(0, Obstacles.Length)];
        Quaternion rotation = Quaternion.Euler(0, 0, 0);
        float objectY = 2.5f;

        // Special case of spawning fire obstacle. Creates a line of fire.
        if (newObstacle.tag == "Fire")
        {
            objectY = 0.5f;
            rotation = Quaternion.Euler(-90, 0, 0);
            newObstacle.transform.Rotate(Vector3.right);
            for (int i = -6; i <= 5; i++)
            {
                createObject(new Vector3(randomX + playerX + 100, objectY, i * 1.5f), rotation, newObstacle);
            }

        }
        else
        {
            createObject(new Vector3(randomX + playerX + 100, objectY, randomZ), rotation, newObstacle);
        }

    }

    // Create obstacle at specified position
    void createObject(Vector3 position, Quaternion rotation, GameObject prefab)
    {
        Instantiate(prefab, position, rotation);
    }

    // Removed obstacles that are out of the game
    void removeObstacle()
    {
        if (obstacleCount > maxObstacles)
        {
            var obstacle = GameObject.FindGameObjectWithTag("obstacle" + (obstacleCount - maxObstacles));
            Object.Destroy(obstacle);

        }
    }

}

