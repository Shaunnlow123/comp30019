﻿using UnityEngine;
using System.Collections;
using System;

public class terrainChunk
{
    public float pixelWidth = 1.0f;
    public float MAX_X { get; private set; }
    public float MIN_X { get; private set; }
    public float MAX_Z { get; private set; }
    public float MIN_Z { get; private set; }

    public GameObject gameObject;
    private Mesh terrainMesh;
    private MeshFilter terrainMeshFilter;
    private MeshRenderer terrainRenderer;
    private MeshCollider terrainCollider;

    private Vector3[] vertices;
    private int[] triangles;
    private int length;

    public terrainChunk(int length, int chunkNumber, Shader shader, Color color)
    {
        this.length = length;
        this.gameObject = new GameObject("terrainChunk" + chunkNumber);
        terrainMeshFilter = this.gameObject.AddComponent<MeshFilter>();
        terrainMeshFilter.mesh = generateTerrain(chunkNumber);
        terrainRenderer = this.gameObject.AddComponent<MeshRenderer>();
        terrainRenderer.material = new Material(Shader.Find("Standard"));
        terrainRenderer.material.color = color;
        terrainCollider = this.gameObject.AddComponent<MeshCollider>();
        terrainCollider.sharedMesh = this.gameObject.GetComponent<MeshFilter>().mesh;
    }

    // terrain generator
    private Mesh generateTerrain(int chunkNumber)
    {

        // Setting initial variables
        int totalPixels = length * length;
        long totalTriangles = (length - 1) * (length - 1) * 6;
        vertices = new Vector3[totalPixels];
        //uv = new Vector2[totalPixels];
        triangles = new int[totalTriangles];

        // Maximum and minimum x and z coodinates for terrain
        MAX_X = (length) * pixelWidth * (chunkNumber-1)  + (length) * pixelWidth / 2;
        MIN_X = (length) * pixelWidth * (chunkNumber - 1) - (length) * pixelWidth / 2;
        //Debug.Log(MAX_X);
        //Debug.Log(MIN_X);
        MAX_Z = (length) * pixelWidth / 2;
        MIN_Z = -(length) * pixelWidth / 2;

        generateVertices(chunkNumber);
        generateTriangles();
        //generateUV(totalPixels);

        // Create mesh for terrain and set its configurations
        terrainMesh = new Mesh();
        terrainMesh.name = "Terrain";
        terrainMesh.vertices = vertices;
        terrainMesh.triangles = triangles;
        terrainMesh.RecalculateNormals();

        return terrainMesh;
    }


    // Generate vertices 
    private void generateVertices(int chunkNumber)
    {
        for (int i = 0; i < length; i++)
        {
            for (int j = 0; j < length; j++)
            {
                vertices[length * i + j] = new Vector3((i * pixelWidth - length * pixelWidth / 2) + 
                    ((length - 1) * pixelWidth * (chunkNumber-1)), 0, j * pixelWidth - length * pixelWidth / 2);

            }
        }
    }

    // Generate triangles
    private void generateTriangles()
    {
        // Vertice index
        int k = 0;

        for (int i = 1; i < length; i++)
        {
            for (int j = 0; j < length - 1; j++)
            {
                triangles[k] = length * i + j;
                triangles[k + 1] = length * (i - 1) + j;
                triangles[k + 2] = length * (i - 1) + (j + 1);
                triangles[k + 3] = length * i + j;
                triangles[k + 4] = length * (i - 1) + (j + 1);
                triangles[k + 5] = length * i + (j + 1);
                k += 6;
            }
        }
    }

    // Generate UV
    /*

    private void generateUV(int totalLength)
    {
        for (int i = 0; i < totalLength - 5; i += 6)
        {
            uv[i] = new Vector2(0.0f, 0.0f); // Top
            uv[i + 1] = new Vector2(0.0f, 1.0f);
            uv[i + 2] = new Vector2(1.0f, 1.0f);
            uv[i + 3] = new Vector2(0.0f, 0.0f);
            uv[i + 4] = new Vector2(1.0f, 1.0f);
            uv[i + 5] = new Vector2(1.0f, 0.0f);


        }
    }
    */


    public void destroyTerrainChunk()
    {
        GameObject.Destroy(this.gameObject);
    }


}


