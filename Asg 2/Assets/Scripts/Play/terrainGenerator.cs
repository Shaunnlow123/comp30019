﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class terrainGenerator : MonoBehaviour
{
    public terrainChunk terrain1;
    public terrainChunk terrain2;

    public PlayerContoller p;

    public Shader shader;
    public Color color;
    private Dictionary<int, terrainChunk> currentChunks { get; set; }

    public int chunks;

    // initial number of tiles
    private int initialChunks = 25;
    public int terrainWidth = 2^4 + 1;

    void Start()
    {
        currentChunks = new Dictionary<int, terrainChunk>();
        createChunks(initialChunks);

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            replaceChunk();
        }
        terrainChunk terrain = this.currentChunks[chunks - initialChunks + 2];

        // endless generation based on character position
        if((p.gameObject.transform.position.x - terrain.MAX_X) > terrainWidth)
        {
            replaceChunk();
            
        }

    }

    // create initial number of chunks
    private void createChunks(int number)
    {
        for (int i = 1; i < number; i++)
        {
            terrainChunk terrain = new terrainChunk(terrainWidth, i, shader, color);
            this.currentChunks.Add(i, terrain);
        }
        this.chunks = number -1;
    }

    // replace chunks at the end and put it to the frnot
    private void replaceChunk()
    {
        terrainChunk terrain = this.currentChunks[chunks - initialChunks + 2];
        
        terrain.destroyTerrainChunk();
        this.currentChunks.Remove(chunks - initialChunks + 2);
        chunks++;
        terrain = new terrainChunk(terrainWidth, chunks, shader, color);
        this.currentChunks.Add(chunks, terrain);
    }


}


