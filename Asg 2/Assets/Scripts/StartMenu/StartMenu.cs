﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour {

    public Button start;
    public Button exit;

	// Use this for initialization
	void Start () {
        start = start.GetComponent<Button>();
        exit = exit.GetComponent<Button>();

	
	}

    public void startPressed()
    {
        SceneManager.LoadScene("main");
    }

    public void exitGame()
    {
        Application.Quit();
    }
	
}
