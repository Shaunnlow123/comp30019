1) What the application does.
The premise of the application is an endless/infinite runner game that revolves around the goal of staying alive as long as possible by avoiding lethal obstacles and gaining points from gems throughout the game. 


2) How to use it.
The player is automatically moving forward and the available controls are moving to the left or right via screen tilting or arrow keys. The player can also jump via tapping the screen or using a spacebar.

3) How you modelled objects and entities.
The terrain is infinitely generated based on an algorithm that replaces the travelled terrain to the end of the current terrain stack. The recycling method creates an illusion that the terrain is seemingly endless as the player travels. The terrain is modelled with a simple mesh and given a simple colour with not textural attributes.

The obstacles and obtainable gems have their models imported from an asset store.

The player is specifically out sourced from mixamo.com which includes available animation and models that provides an accessibility to high quality character models. The animation of the player is manipulated using an animation controller, which alternates between different animation frames based on the character�s state. The controller accept input controls (Accelerometer, touch inputs, keyboard) and manipulate the character�s position.

4) How you handled graphics and camera motion.

The camera is directly following the player in a third person view from a specific, constant distance.
The custom shaders implemented comprises the Blinn-Phong shader, Fresnel shader. The Blinn-Phong shader implements the logic as demonstrated in the lecture and lab content with shadow volumes implemented the cast shadows onto the objects which are using this shader (Rocks and fences). The Fresnel shader implements the Fresnel effect (Both reflection and refraction occurs) to exhibit a �shiny� effect on objects (Gem). The combination of a specific degree of reflection and refraction creates an outer glow to the object and its degree is controlled by factors of Fresnel power, scale and colour. 
A particle system is implemented on the fire obstacle that uses an outsourced model with custom characteristics. A �line� of fire is created by an array of fire prefabs that is randomly generated. 
Unity version 5.4 is used to cancel the need for passing array to shaders that causes various problems.

A change of �unity_WorldToObject and unity_ObjectToWorld to _World2Object and _Object2World respectively� is required to run the shaders correctly on previous versions of Unity.


5) Sources
https://www.assetstore.unity3d.com/en/#!/content/11256 - Campfire pack 
https://www.mixamo.com � Character model and animation
https://www.assetstore.unity3d.com/en/#!/content/36106 - Gem

6) Video
FPS of video is low to ensure that lower end devices and slow internet will not affect accessibility. Actual gameplay has a much better fps.